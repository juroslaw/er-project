"""
usage: python FaceAPITestScript.py
depends on: 'tests/resources/validPhoto1.jpg'
"""

from FaceAPI import FaceAPI
from PhotoCheck import ERImage

er_object = ERImage()
er_object.load_image('tests/resources/aaa.png')
face_api_object = FaceAPI()
print(face_api_object.get_data(er_object))