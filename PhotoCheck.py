from PIL import Image
from PyQt5 import QtWidgets, QtGui


class ERImage:
    """
        Class for loading and validating input image.
    """

    def __init__(self) -> object:
        self.image = None
        self.image_path = str
        self.image_status = 0

    def load_image(self, image_path):
        """
        Validates and displays image from the image_path.
        :param image_path: path to the requested image
        :return: object of class Image or error message is returned
        """
        self.image_path = image_path
        extension_error = self.validate_extension()
        if extension_error is not None:
            self.image_path = None
            return extension_error
        format_error = self.validate_format()
        if format_error is not None:
            self.image_path = None
            return format_error
        self.image_status = 1
        return self.open_image()

    def open_image(self):
        """
        Opens the given image file and returns it as an object of class Image.
        :return: object of class Image or in case of failure error message
        """
        try:
            if self.image_status == 1:
                self.image = Image.open(self.image_path)
            return Image.open(self.image_path)
        except OSError:
            return "Error while loading file, please make sure that the given path is correct."

    def validate_format(self):
        """
        Checks width and height of an opened image.
        :return: error message or None is returned
        """
        format_dictionary = {
            'min-width': 400,
            'min-height': 300,
            'max-width': 1920,
            'max-height': 1080,
            'max_aspect_ratio': 3,  # width/height
            'min_aspect_ratio': 0.3  # width/height
        }
        try:
            image_width = self.get_image_width()
            image_height = self.get_image_height()
            if isinstance(image_height, int) and isinstance(image_width, int):
                if not (format_dictionary['min-width'] <= image_width <= format_dictionary['max-width']) or not (
                        format_dictionary['min-height'] <= image_height <= format_dictionary['max-height']) or not (
                        format_dictionary['min_aspect_ratio'] <= image_width / image_height
                        <= format_dictionary['max_aspect_ratio']):
                    raise OSError
        except OSError:
            return "Please check dimensions of the chosen photo. Minimum dimension 400x300. Maximum dimension 1920x1080"

    def validate_extension(self):
        """
        Checks if the given file extension is supported.
        Supported extensions: bmp, dib, jpg, jpeg, jpe, jfif, tif, tiff, png
        :return: error message or None is returned
        """
        try:
            extension = self.get_file_extension()
            if len(extension) < 5:
                if self.get_file_extension() not in set(
                        ['bmp', 'dib', 'jpg', 'jpeg', 'jpe', 'jfif', 'tif', 'tiff', 'png']):
                    raise OSError
            else:
                return extension
        except OSError:
            return ("File with incorrect extension was given. "
                    "Supported extensions: bmp, dib, jpg, jpeg, jpe, jfif, tif, tiff, png")

    def get_file_extension(self):
        """
        Gets and returns file extension as a string.
        :return: file extension as a string or in case of failure error message
        """
        try:
            if '.' not in self.image_path:
                raise SyntaxError
            else:
                return self.image_path.rsplit('.', 1)[1].lower()
        except SyntaxError:
            return "File without extension was given."

    def get_image_width(self):
        """
        Returns loaded image width in px.
        :return: image width or in case of failure error message
        """
        try:
            return self.open_image().size[0]
        except AttributeError:
            return "Something went wrong, please try again"

    def get_image_height(self):
        """
        Returns loaded image height in px.
        :return: image height or in case of failure error message
        """
        try:
            return self.open_image().size[1]
        except AttributeError:
            return "Something went wrong, please try again"

    def get_image_info(self):
        """
        Returns a dictionary holding data associated with the image.
        :return: dict with info
        """
        return self.open_image().info

    def open_file_dialog(self):
        """
        This function opens file Dialog, where user can choose an image to upload.
        After the file is chosen, it is rescaled and displayed in the main window
        :return: object of class Image or error message is returned
        """
        file_path, file_types = QtWidgets.QFileDialog.getOpenFileName(None, "getOpenFileName()", "",
                                                                      "Image Files(*.bmp *.dib *.jpg *.jpeg *.jpe *.jfif *.tif *.tiff *.png);;")
        if file_path:
            self.image_status = 0
            # loading image object after the verification of format and extension
            return self.load_image(file_path)
