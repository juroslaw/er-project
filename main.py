import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from GUI.FrmMainWindow import UiFrmMainWindow
from PhotoCheck import ERImage
from PIL.ImageQt import ImageQt
from GUI import Error
from GUI import Analysis
from FaceAPI import FaceAPI


class MainWindow(QtWidgets.QMainWindow, UiFrmMainWindow):
    er_image_object: ERImage

    def __init__(self):
        super().__init__()
        self.setup_ui(self)
        self.pixmap_image_object = QtGui.QPixmap()
        self.er_image_object = ERImage()
        self.face_api_object = FaceAPI()
        self.original_er_image = None
        self.setFixedSize(self.size())

    def analysis(self):
        """
        Gets the original ERImage, rotates it, resizes with the given zoom value and
        finally runs FaceAPI and opens a new window with image and results.
        """
        # Get original ERImage
        self.er_image_object.image = self.original_er_image
        # Rotates the ERImage
        self.er_image_object.image = self.er_image_object.image.rotate(self.num_rotation.value(), expand=True)
        
        # Determine if zoom will increase or decrease the image size
        """if float(self.txt_zoom.text()) < 0.0:
            percentage = 1.0 - abs(float(self.txt_zoom.text())) / 100.0
        else:
            percentage = 1.0 + float(self.txt_zoom.text()) / 100.0

        new_size = int(self.er_image_object.image.size[0] * percentage), int(
            self.er_image_object.image.size[1] * percentage)
        # Resize the ERImage
        """
        zoom_value = 1 - int(self.txt_zoom.text())*0.02
        self.er_image_object.image = self.er_image_object.image.crop(tuple(i * zoom_value for i in self.er_image_object.image.getbbox()))
        QtImageObject = ImageQt(self.er_image_object.image)
        # Store ERImage as pixmap
        pixmap_image_object = QtGui.QPixmap.fromImage(QtImageObject).scaled(QtImageObject.size().width() - 2,
                                                                            QtImageObject.size().height() - 2,
                                                                            QtCore.Qt.KeepAspectRatio)
        # run FaceAPI for ERImage
        result = self.face_api_object.get_data(self.er_image_object)
        if type(result) == str:
            Error.component(result)
            UiFrmMainWindow.reset_values(self)
        else:
            emotion = result['emotion']
            face_rectangle = result['face_rectangle']
            self.draw_rectangle_on_the_pixmap(pixmap_image_object=pixmap_image_object, face_rectangle=face_rectangle)
            Analysis.AnalysisWindow(pixmap_image_object, emotion)

    def load_image(self):
        """
        Opens ERImageObject file dialog and loads requested image into an ImageQt object,
        then scales it to the correct display size and sets the scene.
        :param None
        :return None
        """
        loadedImage = self.er_image_object.open_file_dialog()
        if type(loadedImage) == str:
            Error.component(loadedImage)
            UiFrmMainWindow.reset_values(self)

        elif self.er_image_object.image is None:
            print("Photo not chosen!")
        else:
            self.original_er_image = self.er_image_object.image
            QtImageObject = ImageQt(self.er_image_object.image)
            # off-screen image representation that can be used as a paint device  
            self.pixmap_image_object = QtGui.QPixmap.fromImage(QtImageObject).scaled(QtImageObject.size().width() - 2,
                                                                                     QtImageObject.size().height() - 2,
                                                                                     QtCore.Qt.KeepAspectRatio)

            self.scaled_pixmap_main_window(pixmap_image_object=self.pixmap_image_object)
            self.set_image_scene(pixmap_image_object=self.pixmap_image_object_scaled_main_window)
            self.btn_start_analysis.setEnabled(True)
            self.num_rotation.setValue(0)
            self.num_rotation.setEnabled(True)
            self.circ_rotation.setEnabled(True)
            self.txt_zoom.setText("0")
            self.txt_zoom.setEnabled(False)
            self.btn_zoom_in.setEnabled(True)
            self.btn_zoom_out.setEnabled(True)

    def set_image_scene(self, pixmap_image_object):
        """
        Creates scene with loaded pixmap object and displays it in the MainWindow.
        :param None
        :return None
        """
        if self.pixmap_image_object is None:
            Error.component("Something went wrong! None type image detected.")
            UiFrmMainWindow.reset_values(self)
        else:
            # scene object which will display image
            scene = QtWidgets.QGraphicsScene()
            scene.addPixmap(pixmap_image_object)
            self.img_photo.setScene(scene)
            # to draw image in the middle of scene we need the equation PixmapCenterHeight-height/2, PixmapCenterWidth - width/2
            self.img_photo.setGeometry(
                QtCore.QRect(205 - pixmap_image_object.width() / 2, 170 - pixmap_image_object.height() / 2,
                             pixmap_image_object.width() + 2, pixmap_image_object.height() + 2))

    def draw_rectangle_on_the_pixmap(self, pixmap_image_object, face_rectangle):
        """
        Draws a rectangle on the Pixmap on the requested coordinates.
        :param None
        :return None
        """
        if pixmap_image_object is None:
            Error.component("Something went wrong! None type image detected.")
            UiFrmMainWindow.reset_values(self)
        else:
            pen = QtGui.QPen(QtGui.QColor(239, 0, 0), 6)
            painter = QtGui.QPainter(pixmap_image_object)
            painter.setPen(pen)
            painter.drawRect(face_rectangle['left'], face_rectangle['top'], face_rectangle['width'],
                             face_rectangle['height'])
            painter.end()

    def scaled_pixmap_main_window(self, pixmap_image_object):
        """
        Scale given pixmap object to the best possible size with photo ratio consideration to be displayed in the MainWindow.
        :param pixmap_image_object: pixmap image object that will be scaled
        :return None
        """
        self.pixmap_image_object_scaled_main_window = pixmap_image_object.scaled(self.img_photo_width - 2,
                                                                                 self.img_photo_height - 2,
                                                                                 QtCore.Qt.KeepAspectRatio)


if __name__ == "__main__":
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("windowsvista"))
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
