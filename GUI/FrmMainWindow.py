from PyQt5 import QtCore, QtGui, QtWidgets
from PIL.ImageQt import ImageQt
from GUI import Error

"""
    Class with main window of application.
"""


class UiFrmMainWindow(object):
    """
        Function in which window elements are set up.
    """

    def setup_ui(self, frm_main_window):
        self.img_photo_width = 411
        self.img_photo_height = 341
        frm_main_window.setObjectName("FrmMainWindow")
        frm_main_window.resize(920, 420)
        frm_main_window.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        frm_main_window.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        frm_main_window.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.central_widget = QtWidgets.QWidget(frm_main_window)
        self.central_widget.setObjectName("centralWidget")
        self.img_photo = QtWidgets.QGraphicsView(self.central_widget)
        self.img_photo.setGeometry(QtCore.QRect(10, 10, self.img_photo_width, self.img_photo_height))
        self.img_photo.setObjectName("imgPhoto")
        self.btn_update_photo = QtWidgets.QPushButton("Upload Photo", self)
        self.btn_update_photo.setGeometry(620, 330, 121, 51)
        self.btn_update_photo.setObjectName("btnUpdatePhoto")
        self.btn_update_photo.clicked.connect(self.load_image)
        self.label = QtWidgets.QLabel(self.central_widget)
        self.label.setGeometry(QtCore.QRect(500, 30, 81, 16))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.tab_tool_box = QtWidgets.QTabWidget(self.central_widget)
        self.tab_tool_box.setGeometry(QtCore.QRect(470, 60, 391, 231))
        self.tab_tool_box.setObjectName("TabToolBox")
        self.rotation = QtWidgets.QWidget()
        self.rotation.setObjectName("Rotation")
        self.circ_rotation = QtWidgets.QDial(self.rotation)
        self.circ_rotation.setGeometry(QtCore.QRect(90, 10, 131, 151))
        self.circ_rotation.setMaximum(359)
        self.circ_rotation.setObjectName("circRotation")
        self.circ_rotation.setEnabled(False)
        self.num_rotation = QtWidgets.QSpinBox(self.rotation)
        self.num_rotation.setGeometry(QtCore.QRect(240, 70, 61, 31))
        self.num_rotation.setMaximum(359)
        self.num_rotation.setObjectName("numRotation")
        self.num_rotation.setEnabled(False)
        self.tab_tool_box.addTab(self.rotation, "")
        self.zoom = QtWidgets.QWidget()
        self.zoom.setObjectName("Zoom")
        self.btn_zoom_in = QtWidgets.QPushButton(self.zoom)
        self.btn_zoom_in.setGeometry(QtCore.QRect(160, 90, 41, 31))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.btn_zoom_in.setFont(font)
        self.btn_zoom_in.setObjectName("btnZoomIn")
        self.btn_zoom_in.setEnabled(False)
        self.btn_zoom_out = QtWidgets.QPushButton(self.zoom)
        self.btn_zoom_out.setGeometry(QtCore.QRect(220, 90, 41, 32))
        self.btn_zoom_out.setEnabled(False)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.btn_zoom_out.setFont(font)
        self.btn_zoom_out.setObjectName("btnZoomOut")
        self.txt_zoom = QtWidgets.QLineEdit(self.zoom)
        self.txt_zoom.setText('0')
        self.txt_zoom.setGeometry(QtCore.QRect(150, 60, 113, 31))
        self.txt_zoom.setObjectName("txtZoom")
        self.txt_zoom.setEnabled(False)
        self.tab_tool_box.addTab(self.zoom, "")
        self.btn_start_analysis = QtWidgets.QPushButton(self)
        self.btn_start_analysis.setGeometry(QtCore.QRect(750, 330, 121, 51))
        self.btn_start_analysis.setObjectName("btnStartAnalysis")
        self.btn_start_analysis.clicked.connect(self.analysis)
        self.btn_start_analysis.setEnabled(False)
        frm_main_window.setCentralWidget(self.central_widget)
        self.menu_bar = QtWidgets.QMenuBar(frm_main_window)
        self.menu_bar.setGeometry(QtCore.QRect(0, 0, 920, 22))
        self.menu_bar.setObjectName("menuBar")
        frm_main_window.setMenuBar(self.menu_bar)
        self.mainToolBar = QtWidgets.QToolBar(frm_main_window)
        self.mainToolBar.setObjectName("mainToolBar")
        frm_main_window.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(frm_main_window)
        self.statusBar.setObjectName("statusBar")
        frm_main_window.setStatusBar(self.statusBar)
        self.btn_zoom_in.clicked.connect(self.increase_one_point_zoom)
        self.btn_zoom_out.clicked.connect(self.decrease_one_point_zoom)
        self.retranslate_ui(frm_main_window)
        self.tab_tool_box.setCurrentIndex(2)

        self.circ_rotation.valueChanged['int'].connect(self.num_rotation.setValue)
        self.circ_rotation.valueChanged['int'].connect(self.rotate_photo)

        self.num_rotation.valueChanged['int'].connect(self.circ_rotation.setValue)
        QtCore.QMetaObject.connectSlotsByName(frm_main_window)

    def rotate_photo(self):
        """
        Rotates given image and stores resultant pixmap in a setScaledPixmap object, which is displayed in the MainWindow.
        Displays error message if failed.
        """
        if self.er_image_object.image_status == 1:
            # Start the rotation with the original ERImage
            zoom_value = 1 - int(self.txt_zoom.text())*0.02
            rotated_er_image = self.original_er_image.crop(tuple(i * zoom_value for i in self.original_er_image.getbbox()))
            rotated_er_image = rotated_er_image.rotate(self.num_rotation.value(), expand=True)
            QtImageObject = ImageQt(rotated_er_image)
            pixmap_image_Object = QtGui.QPixmap.fromImage(QtImageObject).scaled(QtImageObject.size().width() - 2,
                                                                              QtImageObject.size().height() - 2,
                                                                              QtCore.Qt.KeepAspectRatio)
            self.scaled_pixmap_main_window(
                pixmap_image_object=pixmap_image_Object.scaled(self.img_photo_width - 2, self.img_photo_height - 2,
                                                             QtCore.Qt.KeepAspectRatio))
            self.set_image_scene(pixmap_image_object=self.pixmap_image_object_scaled_main_window)
        else:
            Error.component("No photo chosen so can not rotate")

    def increase_one_point_zoom(self):
        """
        Increases zoom percentage by one point.
        """
        if self.er_image_object.image_status == 1:
            try:
                zoomInputText = int(self.txt_zoom.text())
                if zoomInputText > 19:
                    Error.component("Maximum zoom value is 20")
                    self.txt_zoom.setText("20")
                else:
                    self.txt_zoom.setText(str(zoomInputText + 1))
                    zoom_value = 1 - int(self.txt_zoom.text())*0.02
                    zoomed_image = self.original_er_image.crop(tuple(i * zoom_value for i in self.original_er_image.getbbox()))
                    zoomed_image = zoomed_image.rotate(self.num_rotation.value(), expand=True)
                    zoomed_image.load()
                    QtImageObject = ImageQt(zoomed_image)
                    pixmap_image_Object = QtGui.QPixmap.fromImage(QtImageObject).scaled(QtImageObject.size().width() - 2,
                                                                                        QtImageObject.size().height() - 2,
                                                                                        QtCore.Qt.KeepAspectRatio)
                    self.scaled_pixmap_main_window(pixmap_image_object=pixmap_image_Object.scaled(self.img_photo_width - 2,
                                                                                                  self.img_photo_height - 2, 
                                                                                                  QtCore.Qt.KeepAspectRatio))
                    self.set_image_scene(pixmap_image_object=self.pixmap_image_object_scaled_main_window)
            except ValueError as e:
                Error.component(str(e))
                self.txt_zoom.setText("0")
        else:
            Error.component("No photo chosen so can not zoom in")

    def decrease_one_point_zoom(self):
        """
        Decreases zoom percentage by one point.
        """
        if self.er_image_object.image_status == 1:
            try:
                zoomInputText = int(self.txt_zoom.text())
                if zoomInputText < 1:
                    Error.component("Minimum zoom value is 0")
                    self.txt_zoom.setText("0")
                else:
                    self.txt_zoom.setText(str(zoomInputText - 1))
                    zoom_value = 1 - int(self.txt_zoom.text())*0.02
                    zoomed_image = self.original_er_image.crop(tuple(i * zoom_value for i in self.original_er_image.getbbox()))
                    zoomed_image = zoomed_image.rotate(self.num_rotation.value(), expand=True)
                    zoomed_image.load()
                    QtImageObject = ImageQt(zoomed_image)
                    pixmap_image_Object = QtGui.QPixmap.fromImage(QtImageObject).scaled(QtImageObject.size().width() - 2,
                                                                                        QtImageObject.size().height() - 2,
                                                                                        QtCore.Qt.KeepAspectRatio)
                    self.scaled_pixmap_main_window(pixmap_image_object=pixmap_image_Object.scaled(self.img_photo_width - 2,
                                                                                                  self.img_photo_height - 2, 
                                                                                                  QtCore.Qt.KeepAspectRatio))
                    self.set_image_scene(pixmap_image_object=self.pixmap_image_object_scaled_main_window)
            except ValueError as e:
                Error.component(str(e))
                self.txt_zoom.setText("0")
        else:
            Error.component("No photo chosen so can not zoom out")

    def retranslate_ui(self, frm_main_window):
        _translate = QtCore.QCoreApplication.translate
        frm_main_window.setWindowTitle(_translate("FrmMainWindow", "Emotion Recognition"))
        self.label.setText(_translate("FrmMainWindow", "Toolbox"))
        self.tab_tool_box.setTabText(self.tab_tool_box.indexOf(self.rotation), _translate("FrmMainWindow", "Rotation"))

        self.btn_zoom_in.setText(_translate("FrmMainWindow", "+"))
        self.btn_zoom_out.setText(_translate("FrmMainWindow", "-"))

        self.tab_tool_box.setTabText(self.tab_tool_box.indexOf(self.zoom), _translate("FrmMainWindow", "Zoom"))
        self.btn_start_analysis.setText(_translate("FrmMainWindow", "Start Analysis"))

    def reset_values(self):
        self.er_image_object.image = None
        self.er_image_object.image_status = 0
        self.img_photo.setGeometry(QtCore.QRect(10, 10, self.img_photo_width, self.img_photo_height))
        self.num_rotation.setEnabled(False)
        self.circ_rotation.setEnabled(False)
        self.txt_zoom.setText("0")
        self.txt_zoom.setEnabled(False)
        self.btn_zoom_in.setEnabled(False)
        self.btn_zoom_out.setEnabled(False)
        self.img_photo.setScene(None)
        self.btn_start_analysis.setEnabled(False)


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    FrmMainWindow = QtWidgets.QMainWindow()
    ui = UiFrmMainWindow()
    ui.setup_ui(FrmMainWindow)
    FrmMainWindow.show()
    sys.exit(app.exec_())
