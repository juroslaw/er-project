from PyQt5 import QtGui, QtWidgets, QtCore


def AnalysisWindow(pixmap_image_object, recognize_emotion):
    """
        Function which generates a window with information about the analysis image.
    """

    """
        Setup attributes of window.
    """
    window = QtWidgets.QDialog(None, QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
    window.setWindowTitle("Analysis")
    title = QtWidgets.QLabel(window)
    emotion = QtWidgets.QLabel(window)

    """
        Show analysis image in window in specific position.
    """
    img = pixmap_image_object
    img = img.scaledToWidth(411)
    frame = QtWidgets.QLabel(window)
    frame.setPixmap(img)
    frame.setGeometry(20, 60, img.width(), img.height())

    """
        Setup font size and bold type for title.
    """
    font_title = QtGui.QFont()
    font_title.setPointSize(18)
    font_title.setBold(2)

    """
        Setup title position and text.
    """
    title.setGeometry(20, 10, 300, 40)
    title.setText("Detected emotion(s):")
    title.setFont(font_title)

    """
        Show recognized emotion.
    """
    emotion.setStyleSheet('color: red')
    emotion.setFont(font_title)
    emotion.setGeometry(280, 15, 320, 35)
    emotion.setText(recognize_emotion)
    emotion.setWordWrap(1)

    window.exec()
