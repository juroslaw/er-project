from PyQt5 import QtGui, QtWidgets, QtCore


def component(error_text):
    """
        Function which generates a window with information about the error.
    """
    error_window = QtWidgets.QDialog(None, QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
    error_window.setWindowTitle("Error")

    """
        Font is set up.
    """
    font_title = QtGui.QFont()
    font_title.setPointSize(21)
    font_title.setBold(2)

    """
         Show window title and set position.
    """
    title = QtWidgets.QLabel(error_window)
    title.setGeometry(110, 45, 271, 30)
    title.setText("Error")
    title.setFont(font_title)

    """
         Pass error message to window, the content of the message is determined when the function is called.
    """
    if error_text == "FaceAPI response is empty.":
        error_text = "Face could not be detected."

    description = QtWidgets.QLabel(error_window)
    description.setGeometry(110, 70, 371, 41)
    description.setText(error_text)
    description.setWordWrap(1)

    """
         Add ico to window in specific position.
    """
    frame = QtWidgets.QLabel(error_window)
    img = QtGui.QPixmap('GUI/resources/error.png')
    frame.setPixmap(img)
    frame.setGeometry(20, 30, 80, 80)

    """
         Add button to window in specific position.
    """
    butn_ok = QtWidgets.QPushButton("OK", error_window)
    butn_ok.move(400, 120)
    butn_ok.clicked.connect(error_window.accept)
    error_window.exec()

