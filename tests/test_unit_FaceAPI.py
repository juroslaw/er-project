import unittest


def get_emotion_mock(face_api_response):
    """
    Mocked function that returns name of the identified emotion.
    :param face_api_response: object containing mocked response from the FaceAPI
    :return: string object containing name of the identified emotion
    """
    information = face_api_response
    emotion_analysis = information[0].get('faceAttributes').get('emotion')
    emotion_list = list()
    for key, value in emotion_analysis.items():
        if value > 0.1:
            emotion_list.append(key)
    if emotion_list:
        emotion_name = ", ".join(str(item).capitalize() for item in emotion_list)
    else:
        emotion_name = "Unknown"
    return emotion_name


def send_request_by_path_mock(face_api_response):
    """
    Function mimics behaviour of "" from FaceAPI.py file.
    This function returns error messages instead of raising exceptions.
    :param face_api_response: object containing mocked response from the FaceAPI
    :return: mocked response from the FaceAPI or error message
    """
    result = face_api_response
    if not result:
        return "FaceAPI response is empty"
    for face in result:
        if not face['faceAttributes']['emotion']:
            return "Emotions not calculated for face: {face['faceId']}"
    return result


class FaceAPIUnitTests(unittest.TestCase):

    def test_happiness_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.0, 'disgust': 0.0, 'fear': 0.0, 'happiness': 1.0,
                             'neutral': 0.0, 'sadness': 0.0, 'surprise': 0.0}}}]
        self.assertEqual("Happiness", get_emotion_mock(face_api_mocked_response))

    def test_contempt_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.9, 'disgust': 0.08, 'fear': 0.06, 'happiness': 0.0,
                             'neutral': 0.01, 'sadness': 0.03, 'surprise': 0.04}}}]
        self.assertEqual("Contempt", get_emotion_mock(face_api_mocked_response))

    def test_disgust_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.03, 'disgust': 0.8, 'fear': 0.06, 'happiness': 0.0,
                             'neutral': 0.01, 'sadness': 0.03, 'surprise': 0.04}}}]
        self.assertEqual("Disgust", get_emotion_mock(face_api_mocked_response))

    def test_fear_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.03, 'disgust': 0.1, 'fear': 0.6, 'happiness': 0.0,
                             'neutral': 0.1, 'sadness': 0.03, 'surprise': 0.04}}}]
        self.assertEqual("Fear", get_emotion_mock(face_api_mocked_response))

    def test_neutral_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.03, 'disgust': 0.1, 'fear': 0.06, 'happiness': 0.0,
                             'neutral': 0.7, 'sadness': 0.03, 'surprise': 0.04}}}]
        self.assertEqual("Neutral", get_emotion_mock(face_api_mocked_response))

    def test_sadness_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.03, 'disgust': 0.1, 'fear': 0.06, 'happiness': 0.0,
                             'neutral': 0.1, 'sadness': 0.8, 'surprise': 0.04}}}]
        self.assertEqual("Sadness", get_emotion_mock(face_api_mocked_response))

    def test_surprise_returned(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.03, 'disgust': 0.1, 'fear': 0.06, 'happiness': 0.0,
                             'neutral': 0.1, 'sadness': 0.03, 'surprise': 0.9}}}]
        self.assertEqual("Surprise", get_emotion_mock(face_api_mocked_response))

    def test_all_emotions_equal(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 1.0, 'contempt': 1.0, 'disgust': 1.0, 'fear': 1.0, 'happiness': 1.0,
                             'neutral': 1.0, 'sadness': 1.0, 'surprise': 1.0}}}]
        self.assertEqual("Anger, Contempt, Disgust, Fear, Happiness, Neutral, Sadness, Surprise",
                         get_emotion_mock(face_api_mocked_response))

    def test_all_emotions_zero(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {'anger': 0.0, 'contempt': 0.0, 'disgust': 0.0, 'fear': 0.0, 'happiness': 0.0,
                             'neutral': 0.0, 'sadness': 0.0, 'surprise': 0.0}}}]
        self.assertEqual("Unknown", get_emotion_mock(face_api_mocked_response))

    def test_no_face_api_response(self):
        face_api_mocked_response = None
        self.assertEqual("FaceAPI response is empty", send_request_by_path_mock(face_api_mocked_response))

    def test_no_emotions_given_in_face_api_response(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {
                 'emotion': {}}}]
        self.assertEqual("Emotions not calculated for face: {face['faceId']}",
                         send_request_by_path_mock(face_api_mocked_response))

    def test_no_emotion_attribute_in_face_api_response(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258},
             'faceAttributes': {}}]
        self.assertRaises(KeyError, send_request_by_path_mock, face_api_mocked_response)

    def test_no_face_attributes_in_face_api_response(self):
        face_api_mocked_response = [
            {'faceId': 'b5ff527a-26c1-4b4d-ad05-bd561d8f8524',
             'faceRectangle': {'top': 241, 'left': 221, 'width': 258, 'height': 258}}]
        self.assertRaises(KeyError, send_request_by_path_mock, face_api_mocked_response)

    def test_empty_face_api_response(self):
        face_api_mocked_response = [{}]
        self.assertRaises(KeyError, send_request_by_path_mock, face_api_mocked_response)


if __name__ == '__main__':
    unittest.main()
