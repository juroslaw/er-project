import unittest
from PhotoCheck import ERImage
import os
from PIL import Image


def validate_format_mock(image_width, image_height):
    """
    Checks width and height of an opened image.
    :return: error message or None is returned
    """
    format_dictionary = {
        'min-width': 400,
        'min-height': 300,
        'max-width': 1920,
        'max-height': 1080,
        'max_aspect_ratio': 3,  # width/height
        'min_aspect_ratio': 0.3  # width/height
    }
    try:
        if isinstance(image_height, int) and isinstance(image_width, int):
            if not (format_dictionary['min-width'] <= image_width <= format_dictionary['max-width']) or not (
                    format_dictionary['min-height'] <= image_height <= format_dictionary['max-height']) or not (
                    format_dictionary['min_aspect_ratio'] <= image_width / image_height
                    <= format_dictionary['max_aspect_ratio']):
                raise OSError
    except OSError:
        return "File with incorrect format was given, please check dimensions of the chosen photo."


class ERImageUnitTests(unittest.TestCase):
    def test_load_image_no_extension(self):
        image_path = os.getcwd() + '/resources/too_small'
        self.assertEqual("File without extension was given.", ERImage().load_image(image_path))

    def test_load_image_incorrect_extension(self):
        image_path = os.getcwd() + '/resources/too_small.pdf'
        self.assertEqual(
            "File with incorrect extension was given. Supported extensions: bmp, dib, jpg, jpeg, jpe, jfif, tif, tiff, png",
            ERImage().load_image(image_path))

    def test_load_image_too_small(self):
        image_path = os.getcwd() + '/resources/too_small.jpg'
        self.assertEqual("File with incorrect format was given, please check dimensions of the chosen photo.",
                         ERImage().load_image(image_path))

    def test_load_image_too_big(self):
        image_path = os.getcwd() + '/resources/too_big.jpg'
        self.assertEqual("File with incorrect format was given, please check dimensions of the chosen photo.",
                         ERImage().load_image(image_path))

    def test_load_image_incorrect_path(self):
        image_path = os.getcwd() + '/too_big.jpg'
        self.assertEqual("Error while loading file, please make sure that the given path is correct.",
                         ERImage().load_image(image_path))

    def test_load_image_valid_file(self):
        image_path = os.getcwd() + '/resources/valid_image_file.jpg'
        image_loaded = ERImage().load_image(image_path)
        self.assertTrue(isinstance(image_loaded, Image.Image))

    def test_load_gif_file(self):
        image_path = os.getcwd() + '/valid_size.gif'
        self.assertEqual(
            "File with incorrect extension was given. Supported extensions: bmp, dib, jpg, jpeg, jpe, jfif, tif, tiff, png",
            ERImage().load_image(image_path))

    def test_for_max_aspect_ratio(self):
        image_width = 1920
        image_height = 300
        self.assertEqual("File with incorrect format was given, please check dimensions of the chosen photo.",
                         validate_format_mock(image_width, image_height))

    def test_for_min_aspect_ratio(self):
        image_width = 300
        image_height = 1920
        self.assertEqual("File with incorrect format was given, please check dimensions of the chosen photo.",
                         validate_format_mock(image_width, image_height))


if __name__ == '__main__':
    unittest.main()
