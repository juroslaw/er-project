import cognitive_face as CF
import os


class FaceAPI:
    KEY = '0c763dbaadcd4eab9aa6b0c0beb50b7a'
    BASE_URL = 'https://northeurope.api.cognitive.microsoft.com/face/v1.0/'
    """
        FaceAPI is an interface class with Azure FaceAPI service
        It is meant to be kept minimal, assuming that the image_path
        provided to get_emotions method is valid.
        Output is parsed for various cases.
    """

    def __init__(self):
        """
        Connects to Azure FaceAPI cloud 
        """
        CF.BaseUrl.set(self.BASE_URL)
        CF.Key.set(self.KEY)
        self.current_file_path = ""

    def _save_tmp_img_file(self, er_image_object):
        """
        Save image object to a file
        :param er_image_object: ERImage class object that is displayed in the gui
        :return: True if successful, else raises IOError describing which function failed
        """
        import uuid
        random_string = uuid.uuid4().hex[::7]
        random_string = random_string + '.' + er_image_object.get_file_extension()
        er_image_object.image.save(random_string)
        return random_string

    def _send_request_by_path(self, image_path):
        """
        Requests analysis for a file with given path
        :param image_path: path to the verified image file
        :return: list object containing faces and associated emotion scores as dictionaries
        or in case of failure string containing error message;
        return object example: 
        [{
            'faceId': 'd04cfae2-f83f-4505-8ee0-4b17d1823c71',
            'faceRectangle': {'top': 242, 'left': 221, 'width': 256, 'height': 256},
            'faceAttributes': {'emotion': 
                {'anger': 0.0, ... , surprise': 0.0}}
        }]

        """
        result = CF.face.detect(image_path, attributes='emotion')
        os.remove(image_path)
        if not result:
            return "FaceAPI response is empty."
        for face in result:
            if not face['faceAttributes']['emotion']:
                return "Emotions not calculated for face: {face['faceId']}."
        return result

    def get_data(self, er_image_object):
        """
        Returns name of the identified emotion.
        :param er_image_object: object displayed in the GUI
        :return: string object containing name of the identified emotion
        """
        image_path = self._save_tmp_img_file(er_image_object)
        information = self._send_request_by_path(image_path)
        if type(information) == str:
            return information
        else:
            if len(information) > 1:
                return "Error! More than one face was detected. Please upload photo with one face only."
            emotion_analysis = information[0].get('faceAttributes').get('emotion')
            face_rectangle = information[0].get('faceRectangle')
            emotion_list = list()
            for key, value in emotion_analysis.items():
                if value > 0.1:
                    emotion_list.append(key)
            if emotion_list:
                emotion_name = ", ".join(str(item).capitalize() for item in emotion_list)
            else:
                emotion_name = "Unknown"
            return {'emotion': emotion_name,
                    'face_rectangle': face_rectangle}
